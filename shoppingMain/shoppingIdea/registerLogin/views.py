from django.shortcuts import render, render_to_response
from models import UserDetail
from forms import MyRegistrationForm
from django.template.context_processors import csrf
from django.http.response import HttpResponseRedirect, Http404, HttpResponse
#from websearch import google
from django.contrib.auth.models import User
import datetime
import uuid

# Create your views here.
def register_user(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        userDetails = UserDetail.objects.all()

        #creationTime = datetime.datetime.now().time()
        #creationTime = creationTime.isoformat()
        if User.objects.filter(username=request.POST['username']).exists():
            return HttpResponse("username exists")

        if User.objects.filter(username=request.POST['emailId']).exists():
            return HttpResponse("Email exists")
        print form.errors, form.is_valid()
        if form.is_valid():
            users = form.save(commit=False)
            users.uuid = uuid.uuid1().int
            users.save()
            return HttpResponseRedirect('/accounts/register_success')
        else:
            # Do something in case if form is not valid
            raise Http404
        args = {}
        args.update(csrf(request))
        args['form'] = MyRegistrationForm()

        return render_to_response('register.html',
                                  args)
    else:
        args = {}
        args.update(csrf(request))
        args['form'] = MyRegistrationForm()

        return render_to_response('register.html',
                                  args)


def register_success(request):
    return render_to_response('register_success.html')


def login(request):
    return render_to_response('login.html')