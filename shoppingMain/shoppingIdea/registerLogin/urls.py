from django.conf.urls import patterns, include, url
from views import register_user,register_success,login
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
        url(r'^register/$', register_user),
        url(r'^register_success/$', register_success),
        url(r'^login/$', login),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()