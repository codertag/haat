from __future__ import unicode_literals
from django.db import models
from django_countries.fields import CountryField
# from enum import Enum

# from django_enumfield import enum

TITLE = (
    ('ST', 'Select Title'),
    ('Mister', 'Mr'),
    ('Misses', 'Mrs'),
    ('Miss', 'Ms')
)


# Create your models here.
class UserDetail(models.Model):
    uuid = models.CharField(unique=True,max_length=128)
    title = models.CharField(max_length=6, choices=TITLE)
    username = models.CharField(max_length=25, unique=True)
    password = models.CharField(max_length=25)
    firstName = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)
    middleName = models.CharField(max_length=25, blank=True, null=True)

    # Need to split dateOfBirth in date(enum),month(enum) and year
    dateOfBirth = models.DateField(blank=True, null=True)
    addressLine1 = models.CharField(max_length=50, blank=True, null=True)
    addressLine2 = models.CharField(max_length=50, blank=True, null=True)
    pin = models.CharField(max_length=10, blank=True, null=True)
    city = models.CharField(max_length=25, blank=True, null=True)
    state = models.CharField(max_length=25, blank=True, null=True)
    country = CountryField(null=True, blank=True, blank_label='(select country)')
    countryCode = models.CharField(max_length=5, blank=True, null=True)
    mobileNumber = models.CharField(max_length=10, blank=True, null=True)
    emailId = models.EmailField(max_length=100)
    maritalStatus = models.CharField(max_length=2, blank=True, null=True)
    creationTime = models.DateTimeField(blank=True, auto_now_add=True)
    updateTime = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=2, blank=True, null=True)

