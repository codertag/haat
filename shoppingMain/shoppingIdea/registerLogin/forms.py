from django import forms
from django.forms import widgets
from models import UserDetail
from django_countries.widgets import CountrySelectWidget
import datetime
import uuid

TITLE = (
    ('ST', 'Select Title'),
    ('Mister', 'Mr'),
    ('Misses', 'Mrs'),
    ('Miss', 'Ms')
)


class MyRegistrationForm(forms.ModelForm):
    title = forms.ChoiceField(choices=TITLE, required=True)
    dateOfBirth = forms.DateField(label="Date of Birth", required=False,
                                  widget=widgets.SelectDateWidget(
                                      years=range(1950, 2050, 1), empty_label=("Year", "Month",
                                                                               "Day"))
                                  )
    emailId = forms.EmailField(label="Email Address", help_text="someone@example.com")

    class Meta:
        model = UserDetail
        username = forms.CharField(label="Username")
        widgets = {
            'password': forms.PasswordInput(attrs={'placeholder': 'Enter Password'}),
            'firstName': forms.TextInput(attrs={'placeholder': 'Enter First Name'}),
            'lastName': forms.TextInput(attrs={'placeholder': 'Enter Last Name'}),
            'middleName': forms.TextInput(attrs={'placeholder': 'Enter Middle Name'}),
            'country': CountrySelectWidget()

        }
        fields = ('title', 'username', 'password', 'firstName', 'lastName', 'middleName',
                  'dateOfBirth', 'addressLine1', 'addressLine2', 'pin', 'city', 'state',
                  'country', 'countryCode', 'mobileNumber', 'emailId', 'maritalStatus',
                  )
        #print users.uuid
        #try:
        #    user = model.objects.exclude(pk=self.instance.pk).get(username=username)
        # except user.DoesNotExist:
        #     pass
        # raise forms.ValidationError(u'Username "%s" is already in use.' % username)

        def save(self, commit=True):
            user = super(MyRegistrationForm,self).save(commit=False)
            #if user.objects.filter(username=self.cleaned_data['username']).exists():
            #    print "username exists"
            #    return 0
            #else:
            #    pass
            if commit:
                user.save()
            return user



